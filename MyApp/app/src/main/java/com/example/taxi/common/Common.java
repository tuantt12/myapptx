package com.example.taxi.common;

import com.example.taxi.remote.IGoogleAPI;
import com.example.taxi.remote.RetrofitClient;

public class Common {


    public static final String driverInformation="DriversInformation";
    public static final String CustomerInformation="CustomerInformation";
    public static final String driver="Drivers";
    public static final String pickup_request="PickUpRequest";


    public static final String EMAIL_PATTERN =
            "^[a-zA-Z][\\w-]+@([\\w]+\\.[\\w]+|[\\w]+\\.[\\w]{2,}\\.[\\w]{2,})$";

    public static final String baseUrl="https://maps.googleapis.com";
    public static IGoogleAPI getGoogleApi(){
        return RetrofitClient.getClient(baseUrl).create(IGoogleAPI.class);
    }

}
