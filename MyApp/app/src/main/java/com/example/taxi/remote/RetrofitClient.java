package com.example.taxi.remote;

import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitClient {
    private static Retrofit retrofit = null;


    //getClient
    public static Retrofit getClient(String baseURl) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder().baseUrl(baseURl)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();
        }
        return retrofit;
    }


}
