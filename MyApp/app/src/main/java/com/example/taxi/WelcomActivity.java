package com.example.taxi;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.example.taxi.common.Common;
import com.example.taxi.remote.IGoogleAPI;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.SquareCap;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WelcomActivity extends FragmentActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleMap mMap;
    //play service
    private static final int MY_PERMISSION_REQUEST_CODE = 7000;
    private static final int PLAY_SERVICE_REQ_CODE = 7001;

    // private LocationRequest mLocationRequest;
    LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;

    private static int UPDATE_INTERVAL = 5000;
    private static int FATEST_INTERVAL = 5000;
    private static int DISPLACEMENT = 5000;

    DatabaseReference mDrivers;
    GeoFire geoFire;
    Marker mCurnet;

    SwitchCompat location_Switch;

    SupportMapFragment mapFragment;

    // car animation


    private List<LatLng> polyLineList;
    private Marker carMaker;
    private float v;
    private double lat, lng;
    private Handler handler;
    private LatLng curentPostion, startPostion, endPostion;
    private int index, next;
    private Button btnGo;
    private EditText edPlace;
    private String destination;
    PolylineOptions polylineOptions, blackPolylineOptions;
    Polyline blackpPolyline, grayPolyline;

    IGoogleAPI service;


    /*Runnable drawPath = new Runnable() {
        @Override
        public void run() {
            if (index < polyLineList.size() - 1) {
                index++;
                next = index + 1;
            }
            if (index < polyLineList.size() - 1) {
                startPostion = polyLineList.get(index);
                endPostion = polyLineList.get(next);
            }
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0,1);
            valueAnimator.setDuration(1000);
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    v = valueAnimator.getAnimatedFraction();
                    lng = v * endPostion.longitude + (1 - v) * startPostion.longitude;
                    lat = v * endPostion.latitude + (1 - v) * startPostion.latitude;
                    LatLng point = new LatLng(lat, lng);
                    carMaker.setPosition(point);
                    carMaker.setRotation(getBearing(startPostion, point));
                    mMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder().target(point).zoom(15.5f).build()));

                }
            });
            valueAnimator.start();
            handler.postDelayed(this, 3000);

        }
    };*/

    private float getBearing(LatLng startPostion, LatLng endPostion) {
        double lat = Math.abs(startPostion.latitude - endPostion.latitude);
        double lng = Math.abs(startPostion.longitude - endPostion.longitude);
        if (startPostion.latitude >= endPostion.latitude && startPostion.longitude < endPostion.longitude) {
            return (float) Math.toDegrees(Math.atan(lng / lat));
        } else if (startPostion.latitude < endPostion.latitude && startPostion.longitude < endPostion.longitude) {
            return (float) (90 - Math.toDegrees(Math.atan(lng / lat)) + 90);
        } else if (startPostion.latitude >= endPostion.latitude && startPostion.longitude >= endPostion.longitude) {
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        } else if (startPostion.latitude < endPostion.latitude && startPostion.longitude >= endPostion.longitude)
            return (float) (90 - Math.toDegrees(Math.atan(lng / lat))+270);

        return -1;
    }

        @Override
        protected void onCreate (Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_maps);
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);


            //intit view

            btnGo = findViewById(R.id.btnGo);
            edPlace = findViewById(R.id.ediPlace);
            polyLineList = new ArrayList<>();
            btnGo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    destination = edPlace.getText().toString();
                    destination=destination.replace(" ", "+");//replace space with + to fetch data
                    Log.d("tedt", "onClick: "+destination);
                    getDrection();
                }
            });


            location_Switch = findViewById(R.id.location_swich);
            location_Switch.setChecked(false);
            location_Switch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (location_Switch.isChecked()) {
                        startLocationUpdate();
                        displayLocation();
                        Snackbar.make(mapFragment.getView(), "is online", Snackbar.LENGTH_LONG).show();

                    } else {
                        stopLoctionUpdate();
                        if(mCurnet!=null){
                            mCurnet.remove();
                            mMap.clear();
                        }

                        Snackbar.make(mapFragment.getView(), "is offline", Snackbar.LENGTH_LONG).show();

                    }

                }
            });

            mDrivers = FirebaseDatabase.getInstance().getReference(Common.driver);
            geoFire = new GeoFire(mDrivers);
            setUpLocation();
            service = Common.getGoogleApi();

        }

        private void getDrection () {
            curentPostion = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            String requestApi = null;
            try {
                requestApi = "https://maps.googleapis.com/maps/api/directions/json?mode=driving&transit_routing_preference=less_driving&origin="+curentPostion.latitude + "," + curentPostion.longitude + "&destination=" + destination + "&key=" + getResources().getString(R.string.google_direction_key);
                Log.d("TAG", "getDrection: " + requestApi);//print request api for debug
                service.getPath(requestApi).enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().toString());
                            JSONArray jsonArray = jsonObject.getJSONArray("routes");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject routes = jsonArray.getJSONObject(i);
                                JSONObject poly = routes.getJSONObject("overview_polyline");
                                String polyline = poly.getString("points");
                                polyLineList = decodePoly(polyline);
                            }

                            //adjusting bounds
                            LatLngBounds.Builder builder = new LatLngBounds.Builder();
                            for (LatLng latLng : polyLineList)
                                builder.include(latLng);
                            LatLngBounds bounds = builder.build();

                            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 2);
                            mMap.animateCamera(cameraUpdate);

                            polylineOptions = new PolylineOptions();
                            polylineOptions.color(Color.GRAY);
                            polylineOptions.width(5);
                            polylineOptions.startCap(new SquareCap());
                            polylineOptions.endCap(new SquareCap());
                            polylineOptions.jointType(JointType.ROUND);
                            polylineOptions.addAll(polyLineList);
                            grayPolyline = mMap.addPolyline(polylineOptions);


                            blackPolylineOptions = new PolylineOptions();
                            blackPolylineOptions.color(Color.BLACK);
                            blackPolylineOptions.width(5);
                            blackPolylineOptions.startCap(new SquareCap());
                            blackPolylineOptions.endCap(new SquareCap());
                            blackPolylineOptions.jointType(JointType.ROUND);
                            blackPolylineOptions.addAll(polyLineList);
                            blackpPolyline = mMap.addPolyline(blackPolylineOptions);

                            mMap.addMarker(new MarkerOptions().position(polyLineList.get(polyLineList.size() - 1))
                                    .title("pickup location").icon (BitmapDescriptorFactory.defaultMarker (BitmapDescriptorFactory.HUE_BLUE)));
                            //animation
                            /*ValueAnimator valueAnimator = ValueAnimator.ofInt(0, 100);
                            valueAnimator.setDuration(100);
                            valueAnimator.setInterpolator(new LinearInterpolator());
                            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                                @Override
                                public void onAnimationUpdate(ValueAnimator animation) {
                                    List<LatLng> point = grayPolyline.getPoints();
                                    int prencentValue = (int) valueAnimator.getAnimatedValue();
                                    int size = point.size();
                                    int newPoint = (int) (size * (prencentValue / 100.0f));
                                    List<LatLng> p = point.subList(0, newPoint);
                                    blackpPolyline.setPoints(p);
                                }
                            });
                            valueAnimator.start();*/
                            carMaker = mMap.addMarker(new MarkerOptions().position(curentPostion)
                                    .flat(true).icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
                           /* handler = new Handler();
                            index = -1;
                            next = 1;
                            handler.postDelayed(drawPath, 3000);*/


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        //
        private List<LatLng> decodePoly (String encoded){

            List<LatLng> poly = new ArrayList<LatLng>();
            int index = 0, len = encoded.length();
            int lat = 0, lng = 0;

            while (index < len) {
                int b, shift = 0, result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lat += dlat;

                shift = 0;
                result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lng += dlng;

                LatLng p = new LatLng((((double) lat / 1E5)),
                        (((double) lng / 1E5)));
                poly.add(p);
            }
            return poly;
        }


        @Override
        public void onRequestPermissionsResult ( int requestCode, @NonNull String[] permissions,
        @NonNull int[] grantResults){
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

            switch (requestCode) {
                case MY_PERMISSION_REQUEST_CODE:
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        if (checkGooglePlayService()) {
                            builGoogleClientApi();
                            createLocationRequest();
                            if (location_Switch.isChecked()) {
                                displayLocation();
                            }

                        }

                    }
            }


        }


        private void setUpLocation () {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                //request run time
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                }, MY_PERMISSION_REQUEST_CODE);

            } else {
                if (checkGooglePlayService()) {
                    builGoogleClientApi();
                    createLocationRequest();
                    if (location_Switch.isChecked()) {
                        displayLocation();
                    }

                }
            }

        }

        public void createLocationRequest () {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(5000);
            mLocationRequest.setInterval(UPDATE_INTERVAL);
            mLocationRequest.setFastestInterval(FATEST_INTERVAL);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
        }

        private void builGoogleClientApi () {
            Context context;
            Api api;
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API).build();
            mGoogleApiClient.connect();
        }

        private boolean checkGooglePlayService () {
            int resultRequest = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
            if (resultRequest != ConnectionResult.SUCCESS) {
                if (GooglePlayServicesUtil.isUserRecoverableError(resultRequest))
                    GooglePlayServicesUtil.getErrorDialog(resultRequest, this, PLAY_SERVICE_REQ_CODE).show();
                else {
                    Toast.makeText(this, "dien thaoi k dc ho tro", Toast.LENGTH_SHORT).show();
                    finish();
                }
                return false;
            }
            return true;

        }

        private void stopLoctionUpdate () {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;

            }
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }


        private void startLocationUpdate () {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;

            }
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }


        private void displayLocation () {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;

            }
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                if (location_Switch.isChecked()) {
                    final double lat = mLastLocation.getLatitude();//lay vi do
                    final double longitude = mLastLocation.getLongitude();//lay kinh do
                    Toast.makeText(WelcomActivity.this, "hello" + String.valueOf(lat), Toast.LENGTH_SHORT).show();
                    mapFragment.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            mMap = googleMap;
                            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                            mMap.setTrafficEnabled(false);
                            mMap.setIndoorEnabled(false);
                            mMap.setBuildingsEnabled(false);
                            mMap.getUiSettings().setZoomControlsEnabled(true);
                            LatLng sydney = new LatLng(lat, longitude);
                            Toast.makeText(WelcomActivity.this, "kinh do" + longitude, Toast.LENGTH_SHORT).show();
                            MarkerOptions options = new MarkerOptions().position(sydney).title("i am here");
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 15.0f));
                            mCurnet = googleMap.addMarker(options);
                        }
                    });

                    //update in firebase
                    geoFire.setLocation(FirebaseAuth.getInstance().getCurrentUser().getUid(), new GeoLocation(lat, longitude), new GeoFire.CompletionListener() {
                        @Override
                        public void onComplete(String key, DatabaseError error) {
                            if (error != null) {
                                System.err.println("There was an error saving the location to GeoFire: " + error);
                            } else {
                                System.out.println("Location saved on server successfully!");
                                Toast.makeText(WelcomActivity.this, "Location save on server sucessfully", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            } else {
                Log.d("Error", "displayLocation: khong the get your location");
            }
        }

        private void romate ( final Marker mCurnet, final float i, GoogleMap mMap){
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final float startRotation = mCurnet.getRotation();
            final long duration = 1500;

            final Interpolator interpolator = new LinearInterpolator();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed / duration);
                    float rot = t * i + (1 - t) * startRotation;
                    mCurnet.setRotation(-rot > 180 ? rot / 2 : rot);
                    if (t < 1.0) {
                        handler.postDelayed(this, 16);
                    }
                }
            });


        }


        @Override
        public void onLocationChanged (Location location){
            mLastLocation = location;
            displayLocation();

        }

        @Override
        public void onConnected (@Nullable Bundle bundle){
            displayLocation();
            startLocationUpdate();
        }

        @Override
        public void onConnectionSuspended ( int i){
            mGoogleApiClient.connect();

        }

        @Override
        public void onConnectionFailed (@NonNull ConnectionResult connectionResult){

        }
    }
